﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StartupServerNew
{
    class Program
    {
        static void Main(string[] args)
        {
            GameLogic.Initiate();
            InGame.StartGame();
        }
    }
}
