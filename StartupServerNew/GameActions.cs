﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StartupServerNew
{
    //Всё отсюда перенести в Player, нафига я это тут реализовал?
    //Напомнить самому себе не кодить после 1-го часу ночи.
    class GameActions
    {
        public static void MoveProjectFromHandToTable(Player player)//CardsTypes.Project card)
        {
            Debug.PrintHand_Projects(player);
            bool card_selected = false;
            while (!card_selected)
            {
                Console.WriteLine("Введите id выбранной вами карты для выкладывания на стол");
                card_selected = player.PlaceProjectCardToTable(CheckCoice(Console.ReadLine()));
                if (!card_selected)
                    Debug.PrintHand_Projects(player, true);
            }
        }

        static int CheckCoice(string temp)
        {
            int t_choice;
            try
            {
                t_choice = Convert.ToInt32(temp);
                return t_choice;
            }
            catch
            {
                Console.WriteLine("Пожалуйста, вводите только цифру");
                return CheckCoice(Console.ReadLine());
            }
        }
    }
}
