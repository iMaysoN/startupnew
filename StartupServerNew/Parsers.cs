﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace StartupServerNew
{
    class Parsers
    {
        public class Projects
        {
            static public Dictionary<int, CardsTypes.Project> CreateDeck(string deckPath_Projects)
            {
                XDocument deckXml = XDocument.Load(deckPath_Projects);
                Dictionary<int, CardsTypes.Project> deck = new Dictionary<int, CardsTypes.Project>();

                foreach (XElement el in deckXml.Root.Elements())
                {
                    CardsTypes.Project card = ReadCardProject(el);
                    deck.Add(card.id, card);
                }
                return deck;
            }


            static CardsTypes.Project ReadCardProject(XElement xe)
            {
                CardsTypes.Project card = new CardsTypes.Project();
                if (xe.Element("name") != null)
                    if (xe.Element("name").Value != "")
                        card.name = xe.Element("name").Value;
                if (xe.Attribute("id") != null)
                    if (xe.Attribute("id").Value != "")
                        card.id = Convert.ToInt32(xe.Attribute("id").Value);
                    else
                    {
                        card.id = -1;
                        Console.WriteLine("Ошибка определения id карты {0}", card);
                    }
                if (xe.Element("canAddFeatures") != null)
                    card.canAddFeatures = Convert.ToBoolean(xe.Element("canAddFeatures").Value);
                int value;
                if (xe.Element("works").Element("content") != null)
                    if (xe.Element("works").Element("content").Value != "")
                    {
                        value = Convert.ToInt32(xe.Element("works").Element("content").Value);
                        if (value > 0)
                        {
                            card.content.isExists = true;
                            card.content.current = 0;
                            card.content.max = value;
                        }
                    }
                if (xe.Element("works").Element("design") != null)
                    if (xe.Element("works").Element("design").Value != "")
                    {
                        value = Convert.ToInt32(xe.Element("works").Element("design").Value);
                        if (value > 0)
                        {
                            card.design.isExists = true;
                            card.design.current = 0;
                            card.design.max = value;
                        }
                    }
                if (xe.Element("works").Element("develop") != null)
                    if (xe.Element("works").Element("develop").Value != "")
                    {
                        value = Convert.ToInt32(xe.Element("works").Element("develop").Value);
                        if (value > 0)
                        {
                            card.develop.isExists = true;
                            card.develop.current = 0;
                            card.develop.max = value;
                        }
                    }
                if (xe.Element("works").Element("manage") != null)
                    if (xe.Element("works").Element("manage").Value != "")
                    {
                        value = Convert.ToInt32(xe.Element("works").Element("manage").Value);
                        if (value > 0)
                        {
                            card.manage.isExists = true;
                            card.manage.current = 0;
                            card.manage.max = value;
                        }
                    }
                if (xe.Element("works").Element("deadline") != null)
                    if (xe.Element("works").Element("deadline").Value != "")
                    {
                        value = Convert.ToInt32(xe.Element("works").Element("deadline").Value);
                        if (value > 0)
                        {
                            card.deadline.isExists = true;
                            card.deadline.current = 0;
                            card.deadline.max = value;
                        }
                    }

/*<card id="2">
    <name>Разработка устройств</name>
    <works>
      <develop>6</develop>
      <manage>3</manage>
      <deadline>5</deadline>
    </works>
	<rewards>
		<regular isExists="true">1</regular>
		<ones isExists="false"/>
	</rewards>
    <events>
	  <problems isExists="false"/>
	  <chanses isExists="false"/>
    </events>
  </card>*/
                if (xe.Element("rewards").Element("regular") != null)
                    if (xe.Element("rewards").Element("regular").Attribute("isExists") != null)
                        if (Convert.ToBoolean(xe.Element("rewards").Element("regular").Attribute("isExists").Value))
                            if (Convert.ToInt32(xe.Element("rewards").Element("regular").Value) > 0)
                            {
                                card.isRegular = 1;
                                card.amountRegular = Convert.ToInt32(xe.Element("rewards").Element("regular").Value);
                            }
                if (xe.Element("rewards").Element("ones") != null)
                    if (xe.Element("rewards").Element("ones").Attribute("isExists") != null)
                        if (Convert.ToBoolean(xe.Element("rewards").Element("ones").Attribute("isExists").Value))
                            if (Convert.ToInt32(xe.Element("rewards").Element("ones").Value) > 0)
                            {
                                card.isOnes = 1;
                                card.amountOnes = Convert.ToInt32(xe.Element("rewards").Element("ones").Value);
                            }
                if (xe.Element("events").Element("problems") != null)
                    if (xe.Element("events").Element("problems").Attribute("isExists") != null)
                        if (Convert.ToBoolean(xe.Element("events").Element("problems").Attribute("isExists").Value))
                            if (Convert.ToInt32(xe.Element("events").Element("problems").Value) > 0)
                            {
                                card.isShoudTakeProblems = true;
                                card.countProblems = Convert.ToInt32(xe.Element("events").Element("problems").Value);
                            }

                if (xe.Element("events").Element("chanses") != null)
                    if (xe.Element("events").Element("chanses").Attribute("isExists") != null)
                        if (Convert.ToBoolean(xe.Element("events").Element("chanses").Attribute("isExists").Value))
                            if (Convert.ToInt32(xe.Element("events").Element("chanses").Value) > 0)
                            {
                                card.isShoudTakeChanses = true;
                                card.countChanses = Convert.ToInt32(xe.Element("events").Element("chanses").Value);
                            }
                
                return card;
            }
        }
        public class Chanses
        {
            static public Dictionary<int, CardsTypes.Chanses> CreateDeck(string deckPath)
            {
                XDocument deckXml = XDocument.Load(deckPath);
                Dictionary<int, CardsTypes.Chanses> deck = new Dictionary<int, CardsTypes.Chanses>();

                foreach (XElement el in deckXml.Root.Elements())
                {
                    CardsTypes.Chanses card = ReadCardChanse(el);
                    deck.Add(card.id, card);
                }
                return deck;
            }

            static CardsTypes.Chanses ReadCardChanse(XElement xe)
            {
                CardsTypes.Chanses card = new CardsTypes.Chanses();
                if (xe.Element("name") != null)
                    if (xe.Element("name").Value != "")
                        card.name = xe.Element("name").Value;
                if (xe.Attribute("id") != null)
                    if (xe.Attribute("id").Value != "")
                        card.id = Convert.ToInt32(xe.Attribute("id").Value);
                    else
                    {
                        card.id = -1;
                        Console.WriteLine("Ошибка определения id карты {0}", card);
                    }
                /*
                if (xe.Element("canAddFeatures") != null)
                    card.canAddFeatures = Convert.ToBoolean(xe.Element("canAddFeatures").Value);
                int value;
                if (xe.Element("works").Element("content") != null)
                    if (xe.Element("works").Element("content").Value != "")
                    {
                        value = Convert.ToInt32(xe.Element("works").Element("content").Value);
                        if (value > 0)
                        {
                            card.content.isExists = true;
                            card.content.current = 0;
                            card.content.max = value;
                        }
                    }
                if (xe.Element("works").Element("design") != null)
                    if (xe.Element("works").Element("design").Value != "")
                    {
                        value = Convert.ToInt32(xe.Element("works").Element("design").Value);
                        if (value > 0)
                        {
                            card.design.isExists = true;
                            card.design.current = 0;
                            card.design.max = value;
                        }
                    }
                if (xe.Element("works").Element("develop") != null)
                    if (xe.Element("works").Element("develop").Value != "")
                    {
                        value = Convert.ToInt32(xe.Element("works").Element("develop").Value);
                        if (value > 0)
                        {
                            card.develop.isExists = true;
                            card.develop.current = 0;
                            card.develop.max = value;
                        }
                    }
                if (xe.Element("works").Element("manage") != null)
                    if (xe.Element("works").Element("manage").Value != "")
                    {
                        value = Convert.ToInt32(xe.Element("works").Element("manage").Value);
                        if (value > 0)
                        {
                            card.manage.isExists = true;
                            card.manage.current = 0;
                            card.manage.max = value;
                        }
                    }
                if (xe.Element("works").Element("deadline") != null)
                    if (xe.Element("works").Element("deadline").Value != "")
                    {
                        value = Convert.ToInt32(xe.Element("works").Element("deadline").Value);
                        if (value > 0)
                        {
                            card.deadline.isExists = true;
                            card.deadline.current = 0;
                            card.deadline.max = value;
                        }
                    }


                if (xe.Element("rewards").Element("regular") != null)
                    if (xe.Element("rewards").Element("regular").Attribute("isExists") != null)
                        if (Convert.ToBoolean(xe.Element("rewards").Element("regular").Attribute("isExists").Value))
                            if (Convert.ToInt32(xe.Element("rewards").Element("regular").Value) > 0)
                            {
                                card.isRegular = 1;
                                card.amountRegular = Convert.ToInt32(xe.Element("rewards").Element("regular").Value);
                            }
                if (xe.Element("rewards").Element("ones") != null)
                    if (xe.Element("rewards").Element("ones").Attribute("isExists") != null)
                        if (Convert.ToBoolean(xe.Element("rewards").Element("ones").Attribute("isExists").Value))
                            if (Convert.ToInt32(xe.Element("rewards").Element("ones").Value) > 0)
                            {
                                card.isRegular = 1;
                                card.amountRegular = Convert.ToInt32(xe.Element("rewards").Element("ones").Value);
                            }
                if (xe.Element("events").Element("problems") != null)
                    if (xe.Element("events").Element("problems").Attribute("isExists") != null)
                        if (Convert.ToBoolean(xe.Element("events").Element("problems").Attribute("isExists").Value))
                            if (Convert.ToInt32(xe.Element("events").Element("problems").Value) > 0)
                            {
                                card.isShoudTakeProblems = true;
                                card.countProblems = Convert.ToInt32(xe.Element("events").Element("problems").Value);
                            }

                if (xe.Element("events").Element("chanses") != null)
                    if (xe.Element("events").Element("chanses").Attribute("isExists") != null)
                        if (Convert.ToBoolean(xe.Element("events").Element("chanses").Attribute("isExists").Value))
                            if (Convert.ToInt32(xe.Element("events").Element("chanses").Value) > 0)
                            {
                                card.isShoudTakeChanses = true;
                                card.countChanses = Convert.ToInt32(xe.Element("events").Element("chanses").Value);
                            }
                */
                return card;
            }
        }
    }
}
