﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StartupServerNew
{
    class InGame
    {
        public static void StartGame()
        {
            bool flag_isVictory = false; //По этому флагу определяем факт победы
            Player winner = new Player(); //Сюда будет записан победитель
            //TODO продумать вариант с несколькими победителями
            int round_counter = 0; //счётчик раунда

            Console.WriteLine("Игра началась!");
            PrintHint();

            while (!flag_isVictory)
            {
                round_counter++;
                Console.WriteLine("------------------------------------");
                Console.WriteLine("Начался раунд №" + round_counter.ToString());
                Console.WriteLine("------------------------------------");
                //Debug.PrintPlayers(Vars.players_list);
                //Ход передаётся каждому игроку по очереди, начиная с нулевого
                foreach (Player player in Vars.players_list)
                {
                    bool playerPlacedProject = false;
                    bool playerPlacedChance = false;
                    bool isPlayerEnd = false;

                    while (!isPlayerEnd)
                    {
                        //Проверяем возможности
                        //Сообщаем о них игроку
                        //Даём ему выбор действия
                        //Проверяем валидность действия
                        //Совершаем действие
                        //Передаём ход

                        Console.WriteLine("Ход игрока " + player.name);
                        Console.WriteLine();
                        Debug.PrintPlayer(player);

                        if ((player.inHand.projects.Count > 0) && (!playerPlacedProject))
                        {
                            Console.WriteLine("--> 1. Вы можете сыграть карту проекта");
                        }
                        if ((player.inHand.chanses.Count > 0) && (!playerPlacedChance))
                        {
                            Console.WriteLine("--> 2. Вы можете сыграть карту возможностей");
                        }
                        if (CheckStaff(player.staff))
                        {
                            Console.WriteLine("--> 3. Отправить сотрудников на проекты");
                            Console.WriteLine();
                        }

                        Console.WriteLine("--> 0. Завершить неделю");

                        //Выбор действия
                        switch (CheckCoice(Console.ReadLine()))
                        {
                            case (1):
                                //Выложить карту проекта на стол
                                GameActions.MoveProjectFromHandToTable(player);
                                playerPlacedProject = true;
                                break;
                            case (2):
                                playerPlacedChance = true;
                                //Сыграть карту возможности
                                break;
                            case (3):
                                //Назначить сотрудника на работу
                                player.SendToWork();
                                break;
                            case (0):
                                //Проверяем доступные возможности игрока и предупреждаем, если таковые будут
                                //Если игрок хочет - завершаем неделю и переходим к следующему
                                isPlayerEnd = true;
                                break;
                            default:
                                break;
                        }
                    }               
                }
                if (round_counter > 10)
                    flag_isVictory = true;
                EndWeek();
            }
        }

        public static void EndWeek()
        {
            //Провести подсчёт бонусов/штрафов
            //получить доходы
            //заплатить зарплаты
            //добавить дедлайнов проектам на столе (выдать чёрных меток за провалы проекта)
            //Уменьшить дебафы
            Console.WriteLine("Конец недели!");
        }

        static bool CheckStaff(List<Staff> staff)
        {
            foreach (Staff t_staff in staff)
            {
                if (t_staff.isWork)
                {
                    return true;
                }
            }
            return false;
        }

        static int CheckCoice(string temp)
        {
            if (temp.Trim().ToLower() == "help")
            {
                PrintHint();
            }
            int t_choice;
            try
            {
                t_choice = Convert.ToInt32(temp);
                return t_choice;
            }
            catch
            {
                Console.WriteLine("Пожалуйста, вводите только цифру");
                return CheckCoice(Console.ReadLine());
            }
        }

        static void PrintHint()
        {
            Console.WriteLine();
            Console.WriteLine("У игрока есть неделя на любые действия.");
            Console.WriteLine("В течении недели он может:");
            Console.WriteLine("  1. Выложить/взять в работу сколь угодно много проектов");
            Console.WriteLine("  2. Сыграть одну карту возможности");
            Console.WriteLine("    2.1. Карта возможности играется либо как возможность, либо как найм сотрудника");
            Console.WriteLine("  3. Ожидать конца недели после завершения ходов всех других игроков");
            Console.WriteLine();
            Console.WriteLine("В конце недели все игроки получают доход от проектов, а потом выплачивают все штрафы/зарплаты/долги");
            Console.WriteLine("Используйте 'help', если забыли эти правила");
            Console.WriteLine();
        }
        //public bool CheckAvailableStaff
    }
}
