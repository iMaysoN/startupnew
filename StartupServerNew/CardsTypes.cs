﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StartupServerNew
{
    class CardsTypes
    {
        public class Project
        {
            public int id;
            public string name;
            public bool canAddFeatures;
            public bool isComplete;

            public CardsParts.Working content = new CardsParts.Working("Контент");
            public CardsParts.Working design = new CardsParts.Working("Дизайн");
            public CardsParts.Working manage = new CardsParts.Working("Менеджмент");
            public CardsParts.Working develop = new CardsParts.Working("Разработка");
            public CardsParts.Working deadline = new CardsParts.Working("Срок");

            public int isRegular;
            public int amountRegular;
            public int isOnes;
            public int amountOnes;

            public bool isShoudTakeChanses;
            public int countChanses;
            public bool isShoudTakeProblems;
            public int countProblems;

            public List<CardsTypes.Chanses> features;

            public Project()
            {
                name = "";
                isComplete = false;
                canAddFeatures = false;
                features = new List<Chanses>();
            }
        }
        
        public class Chanses
        {
            public int id;
            public string name;
        }
        public class Events
        {

        }
    }
}
