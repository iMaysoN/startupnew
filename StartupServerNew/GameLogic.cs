﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StartupServerNew
{
    class GameLogic
    {
        public static void Initiate()
        {
            CreateDecks();
            AddPlayer(2);
            InitTable();
            GiveCardsToPlayers();
            AddPlayerAsEmployee();
            
            Debug.PrintPlayers(Vars.players_list);

            //Debug.PrintDeck.Projects(Vars.game_deck_project);
            //Debug.PrintDeck.Chanses(Vars.game_deck_chanses);

            //InGame.StartGame();
        }

        public static void InitTable()
        {
            Vars.onTable = new Table();
        }

        public static void AddPlayer(int count)
        {
            for (int i=0; i<count; i++)
            {
                Player player = new Player();
                Console.WriteLine("Введите имя игрока: ");
                // player.name = Console.ReadLine().ToString();
                //Debug on
                player.id = Vars.players_list.Count.ToString();
                switch (i)
                {
                    case 0:
                        player.name = "Нулевой " + player.id.ToString();
                        break;
                    case 1:
                        player.name = "Первый " + player.id.ToString();
                        break;
                    case 2:
                        player.name = "Второй " + player.id.ToString();
                        break;
                    case 3:
                        player.name = "Третий " + player.id.ToString();
                        break;
                    default:
                        player.name = "Иной" + player.id.ToString();
                        break;
                }

                
                Console.WriteLine("в дебажных целях имя " + player.id.ToString() + " игрока - " + player.name);
                //Debug off
               
                Vars.players_list.Add(player);
            }
        }

        public static void CreateDecks()
        {
            //deck_projects - общая колода, которая хранит в себе всю инфу о картах проектов в игре
            //не меняется с запуска игры
            Vars.deck_projects = Parsers.Projects.CreateDeck(Paths.path_to_projects);
            //game_deck_project - игровая колода, содержит те карты, которые лежат в колоде
            //game_drop_deck_project - игровая колода, содержит карты проектов в сбросе
            Vars.game_deck_project = new List<CardsTypes.Project>();
            foreach (KeyValuePair<int, CardsTypes.Project> temp in Vars.deck_projects)
                Vars.game_deck_project.Add(temp.Value);
            Vars.game_deck_project = Shuffle.DeckProjects(Vars.game_deck_project);

            Vars.deck_chanses = Parsers.Chanses.CreateDeck(Paths.path_to_chanses);
            Vars.game_deck_chanses = new List<CardsTypes.Chanses>();
            foreach (KeyValuePair<int, CardsTypes.Chanses> temp in Vars.deck_chanses)
                Vars.game_deck_chanses.Add(temp.Value);
            Vars.game_deck_chanses = Shuffle.DeckChanses(Vars.game_deck_chanses);
        }

        public class Shuffle
        {
            static public List<CardsTypes.Project> DeckProjects(List<CardsTypes.Project> deck)
            {
                int length = Convert.ToInt32(deck.Count);
                List<CardsTypes.Project> deck_return = new List<CardsTypes.Project>();
                Random random = new Random();
                List<CardsTypes.Project> deck_reverse = new List<CardsTypes.Project>();
                int ran;
                while (length > 0)
                {
                    ran = random.Next(length);
                    deck_return.Add(deck[ran]);
                    deck.RemoveAt(ran);
                    length = deck.Count;
                }
                return deck_return;
            }

            static public List<CardsTypes.Chanses> DeckChanses(List<CardsTypes.Chanses> deck)
            {
                int length = Convert.ToInt32(deck.Count);
                List<CardsTypes.Chanses> deck_return = new List<CardsTypes.Chanses>();
                Random random = new Random();
                List<CardsTypes.Chanses> deck_reverse = new List<CardsTypes.Chanses>();
                int ran;
                while (length > 0)
                {
                    ran = random.Next(length);
                    deck_return.Add(deck[ran]);
                    deck.RemoveAt(ran);
                    length = deck.Count;
                }
                return deck_return;
            }

            public static void AllDecks()
            {
                Vars.game_deck_project = DeckProjects(Vars.game_deck_project);
                //GlobalVar.deck_problems = ShuffleDeck(GlobalVar.deck_problems);
                //GlobalVar.deck_chanses = ShuffleDeck(GlobalVar.deck_chanses);
            }
        }

        public static void GiveCardsToPlayers()
        {
            foreach (Player player in Vars.players_list)
            {
                GiveCard.Projects(player, 3);
                GiveCard.Chanses(player, 3);
            }
            

            //Vars.players_list[0].inHand.projects.Add(Vars.game_deck_project[0]);
            //Vars.players_list[0].inHand.chanses.Add(Vars.game_deck_chanses[0]);
        }

        public static void AddPlayerAsEmployee()
        {
            Random rand = new Random();

            foreach (Player player in Vars.players_list)
            {
                player.staff.Add(new Staff(Staff.Workers.Player));
                //Staff.Workers workers = (Staff.Workers)rand.Next(Enum.GetValues(typeof(Staff.Workers)).Length - 1);
                //player.staff.Add(new Staff(workers));
            }
        }
    }
}
