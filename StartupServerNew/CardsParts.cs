﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StartupServerNew
{
    class CardsParts
    {
        public class OnTable
        {
            public string isComplete;

        }
        public class Backup
        {
            public int max;
            public int current;
            public bool isReady;

            public Backup()
            {
                max = 2;
                current = 0;
                isReady = false;
            }
        }

        public class Working
        {
            public string name;
            public bool isExists;
            public int max;
            public int current;
            public bool isCompleted;

            public Working()
            {
                name = "";
                isExists = false;
                current = 0;
                max = 0;
                isCompleted = false;
            }

            public Working(string Name)
            {
                name = Name;
                max = 0;
                current = 0;
                isExists = false;
                isCompleted = false;
            }

            public Working(string Name, int Max, bool IsExists, bool IsCompleted)
            {
                name = Name;
                max = Max;
                current = 0;
                isExists = IsExists;
                isCompleted = IsCompleted;
            }

        }
    }
}
