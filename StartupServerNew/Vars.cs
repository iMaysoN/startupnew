﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StartupServerNew
{
    class Vars
    {
        public static List<Player> players_list = new List<Player>();

        public static Dictionary<int, CardsTypes.Project> deck_projects;
        public static Dictionary<int, CardsTypes.Chanses> deck_chanses;
        public static Dictionary<int, CardsTypes.Events> deck_events;

        public static List<CardsTypes.Project> game_deck_project;
        public static List<CardsTypes.Chanses> game_deck_chanses;
        public static List<CardsTypes.Events> game_deck_events;
        //public static ArrayList game_deck_project;

        public static Table onTable;


    }

    class Staff
    {
        public Workers workerType;
        public bool isWork;
        public int sleep;

        public string name;

        public int power;

        //Устарело
        public int design;
        public int content;
        public int develop;
        public int manage;
        public int any;

        public Staff(Workers type)
        {
            workerType = type;
            isWork = true;
            sleep = 0;

            power = 1;

            design = 0;
            content = 0;
            develop = 0;
            manage = 0;
            any = 0;

            switch (type)
            {
                case Workers.Designer:
                    design = 1;
                    name = "Дизайнер";
                    break;
                case Workers.Content:
                    content = 1;
                    name = "Контент";
                    break;
                case Workers.Developer:
                    develop = 1;
                    name = "Разработчик";
                    break;
                case Workers.Manager:
                    manage = 1;
                    name = "Менеджер";
                    break;
                case Workers.Player:
                    any = 1;
                    name = "Игрок";
                    break;
            }
        }

        public void Set_Work()
        {
            if (!isWork)
                if (sleep == 0)
                    isWork = true;
        }

        public void Set_Not_Work(int week=1)
        {
            if (sleep == 0)
            {
                sleep = week;
                isWork = false;
            }
            else
                sleep = sleep + week;

        }

        public enum Workers
        {
            Designer,
            Content,
            Developer,
            Manager,
            Player
        }
    }

    class Table
    {
        public Dictionary<int, CardsTypes.Events> currentProblems;
        
        public Table()
        {
            currentProblems = new Dictionary<int, CardsTypes.Events>();
        }
    }

    class Paths
    {
        public static string path_to_projects = "Deck_Projects.xml";
        public static string path_to_chanses = "Deck_Chanses.xml";
    }
}
