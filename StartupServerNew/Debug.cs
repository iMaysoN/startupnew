﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StartupServerNew
{
    class Debug
    {
        public static void PrintPlayers(List<Player> list)
        {
            foreach (Player pl in list)
            {
                PrintPlayer(pl);
            }
        }
        public static void PrintPlayer(Player pl)
        {
            string id = pl.id;
            string name = pl.name;
            if (id == "")
                id = "null";
            if (name == "")
                name = "null";
            Console.WriteLine("Игрок {0} имеет id={1}", name, id);
            Console.WriteLine("Баланс игрока - {0}, доход в ход - {1}", pl.balance.current, pl.balance.profit);
            if (pl.balance.turn_without_profit < 1)
                Console.WriteLine("Игрок может на этом ходу получать доход от проектов");
            else
                Console.WriteLine("Игрок не получает на этому ходу дохода от проектов");
            Console.WriteLine("У игрока в руках {0} карт Возможностей", pl.inHand.chanses.Count);
            if (pl.inHand.chanses.Count > 0)
            {
                string t_chanses_list_inHand = "";
                t_chanses_list_inHand += "Список карт возможностей:\n";
                foreach (CardsTypes.Chanses t_chanses_card in pl.inHand.chanses)
                {
                    t_chanses_list_inHand += "  " + t_chanses_card.name + "\n";
                }
                Console.WriteLine(t_chanses_list_inHand);
            }
            Console.WriteLine("У игрока в руках {0} карт Проектов", pl.inHand.projects.Count);
            if (pl.inHand.projects.Count > 0)
            {
                string t_projects_list_inHand = "";
                t_projects_list_inHand += "Список карт проектов:\n";
                foreach (CardsTypes.Project t_project_card in pl.inHand.projects)
                {
                    t_projects_list_inHand += "  " + t_project_card.name + "\n";
                }
                Console.WriteLine(t_projects_list_inHand);
            }
            Console.WriteLine("У игрока выложено {0} проектов", pl.projectsOnTable.Count);
            if (pl.projectsOnTable.Count > 0)
                foreach (CardsTypes.Project pr in pl.projectsOnTable)
                    if (pr.isComplete)
                        Console.WriteLine("Проект {0} завершен", pr.name);
                    else
                    {
                        Console.WriteLine("Проекту '{0}' осталось:", pr.name);
                        if (pr.content.isExists)
                            WorkStatus(pr.content);
                        if (pr.manage.isExists)
                            WorkStatus(pr.manage);
                        if (pr.develop.isExists)
                            WorkStatus(pr.develop);
                        if (pr.design.isExists)
                            WorkStatus(pr.design);

                        if (pr.deadline.isExists)
                            if (pr.deadline.current < pr.deadline.max)
                                Console.WriteLine("Осталось {0} недель до истечения срока", (pr.deadline.max - pr.deadline.current).ToString());

                    }
            string empl = "";

            if (pl.staff.Count == 0)
                empl = "сотрудников";
            else
                if (pl.staff.Count < 2)
                    empl = "сотрудник:";
                else
                    if (pl.staff.Count < 5)
                        empl = "сотрудника:";
                    else
                        empl = "сотрудников:";
            

            Console.WriteLine("У игрока {0} {1}", pl.staff.Count, empl);
            if (pl.staff.Count > 0)
                PrintStaffList(pl.staff);

            Console.WriteLine("");
            Console.WriteLine("----");
        }

        static void WorkStatus(CardsParts.Working pr)
        {
            if (!pr.isCompleted)
                Console.WriteLine(" ->Нужно выполнить {0} - {1}", pr.name, (pr.max - pr.current).ToString());
            else
                Console.WriteLine(" ->{0} - выполнено", pr.name);
        }

        public class PrintDeck
        {
            static public void Projects(List<CardsTypes.Project> deck)
            {
                foreach (CardsTypes.Project card in deck)
                {
                    Console.WriteLine("В колоде Проектов остались карты:");
                    Console.WriteLine(card.id + " - " + card.name);
                    Console.WriteLine();
                }
            }
            static public void Chanses(List<CardsTypes.Chanses> deck)
            {
                foreach (CardsTypes.Chanses card in deck)
                {
                    Console.WriteLine("В колоде Проектов остались карты:");
                    Console.WriteLine(card.id + " - " + card.name);
                    Console.WriteLine();
                }
            }
        }

        public static void PrintStaffList(List<Staff> staff)
        {
            foreach (Staff employee in staff)
            {
                string employee_info = "";
                employee_info += "  Сотрудник " + employee.name;
                if (employee.isWork)
                    employee_info += " в рабочем состоянии";
                else
                    employee_info += " не работает " + employee.sleep.ToString() + " ходов";
                Console.WriteLine(employee_info);
            }
        }

        public static void PrintList(List<Staff> staff)
        {
            for (int i=0; i<staff.Count; i++)
            {
                Console.WriteLine("{0}. Сотрудник {1} способен выполнить {2} работы", i, staff[i].name, staff[i].power);
            }
        }

        public static void PrintList(List<CardsTypes.Project> cards)
        {
            for (int i = 0; i < cards.Count; i++)
            {
                Console.WriteLine("{0}. Проект '{1}'", i, cards[i].name);
            }
        }


        static public void PrintHand_Projects(Player pl, bool onlyNames=false)
        {
            foreach (CardsTypes.Project card in pl.inHand.projects)
            {
                Console.WriteLine("------------------------------------");
                PrintCard(card, onlyNames);
                Console.WriteLine("------------------------------------");
            }
        }

        static public void PrintCard(CardsTypes.Project c, bool onlyNames)
        {
            Console.WriteLine("{0}: Имя карты - {1}", c.id, c.name);
            if (!onlyNames)
            {
                if (c.canAddFeatures)
                    Console.WriteLine("На проект можно докрутить дополнительные фичи!");
                else
                    Console.WriteLine("Проект без возможности монетизации");
                if (c.content.isExists)
                    Console.WriteLine("{0}: сделано {1} из {2}", c.content.name, c.content.current, c.content.max);
                if (c.design.isExists)
                    Console.WriteLine("{0}: сделано {1} из {2}", c.design.name, c.design.current, c.design.max);
                if (c.develop.isExists)
                    Console.WriteLine("{0}: сделано {1} из {2}", c.develop.name, c.develop.current, c.develop.max);
                if (c.manage.isExists)
                    Console.WriteLine("{0}: сделано {1} из {2}", c.manage.name, c.manage.current, c.manage.max);
                if (c.deadline.isExists)
                    Console.WriteLine("{0}: сделано {1} из {2}", c.deadline.name, c.deadline.current, c.deadline.max);
                if (c.isRegular > 0)
                    Console.WriteLine("Вы получите {0} голды в ход", c.amountRegular);
                if (c.isOnes > 0)
                    Console.WriteLine("Вы получите {0} голды единоразово", c.amountOnes);
                if (c.isShoudTakeChanses)
                    Console.WriteLine("По окончанию проекта вы должны получить {0} карт Возможности", c.countChanses);
                if (c.isShoudTakeProblems)
                    Console.WriteLine("По окончанию проекта вы должны получить {0} карт Проблемы", c.countProblems);
            }
            Console.WriteLine();
        }
    }
}
