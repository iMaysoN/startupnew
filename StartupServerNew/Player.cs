﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StartupServerNew
{
    class Player
    {
        public string id;
        public string name;
        public GoldBalance balance;
        public InHand inHand;
        public List<CardsTypes.Project> projectsOnTable;
        public CardsParts.Backup backup;
        public List<Staff> staff;


        public Player()
        {
            id = "";
            name = "";
            balance = new GoldBalance();
            balance.current = 10;
            backup = new CardsParts.Backup();
            inHand = new InHand();
            projectsOnTable = new List<CardsTypes.Project>();
            staff = new List<Staff>();
        }

        public bool PlaceProjectCardToTable(int card_id)
        {
            if (IsProjectCardInHand(card_id))
            {
                bool card_moved = false;
                int i = 0;
                while ((i < inHand.projects.Count) && (!card_moved))
                {
                    if (inHand.projects[i].id == card_id)
                    {
                        projectsOnTable.Add(inHand.projects[i]);
                        inHand.projects.RemoveAt(i);
                        card_moved = true;
                    }
                    i++;
                }
                return true;
            }
            else
                return false;
        }

        public void SendToWork()
        {
            List<Staff> list = new List<Staff>();
            foreach (Staff t_staff in this.staff)
            {
                if (t_staff.isWork)
                    list.Add(t_staff);
            }

            int empl_choiced = -1;
            int project_choiced = -1;
            bool job_made;

            while (list.Count > 0)
            {
                job_made = false;
                while (!job_made)
                {
                    Console.WriteLine("Выберите сотрудника");
                    Debug.PrintList(list);
                    empl_choiced = CheckCoice(Console.ReadLine());
                    Debug.PrintList(projectsOnTable);
                    Console.WriteLine("Выберите проект");
                    project_choiced = CheckCoice(Console.ReadLine());

                    //TODO: Придумать нормальную реализацию ужаса ниже
                    switch (list[empl_choiced].workerType)
                    {
                        case (Staff.Workers.Content):
                            if (!projectsOnTable[project_choiced].content.isCompleted)
                            {
                                projectsOnTable[project_choiced].content.current++;
                                if (CheckProjectIsCompleted(projectsOnTable[project_choiced].content))
                                    projectsOnTable[project_choiced].content.isCompleted = true;
                                list.RemoveAt(empl_choiced);
                                job_made = true;
                            }
                            else
                                Console.WriteLine("Простите, но для сотрудника {0} на проекте {1} не нашлось работы", list[empl_choiced].name, projectsOnTable[project_choiced].name);
                            break;
                        case (Staff.Workers.Designer):
                            if (!projectsOnTable[project_choiced].design.isCompleted)
                            {
                                projectsOnTable[project_choiced].design.current++;
                                list.RemoveAt(empl_choiced);
                                job_made = true;
                            }
                            else
                                Console.WriteLine("Простите, но для сотрудника {0} на проекте {1} не нашлось работы", list[empl_choiced].name, projectsOnTable[project_choiced].name);
                            break;
                        case (Staff.Workers.Developer):
                            if (!projectsOnTable[project_choiced].develop.isCompleted)
                            {
                                projectsOnTable[project_choiced].develop.current++;
                                list.RemoveAt(empl_choiced);
                                job_made = true;
                            }
                            else
                                Console.WriteLine("Простите, но для сотрудника {0} на проекте {1} не нашлось работы", list[empl_choiced].name, projectsOnTable[project_choiced].name);
                            break;
                        case (Staff.Workers.Manager):
                            if (!projectsOnTable[project_choiced].manage.isCompleted)
                            {
                                projectsOnTable[project_choiced].manage.current++;
                                list.RemoveAt(empl_choiced);
                                job_made = true;
                            }
                            else
                                Console.WriteLine("Простите, но для сотрудника {0} на проекте {1} не нашлось работы", list[empl_choiced].name, projectsOnTable[project_choiced].name);
                            break;
                        case (Staff.Workers.Player):
                            Console.WriteLine("Выберите тип работы, который хотите выполнить");
                            if (!CheckProjectIsCompleted(projectsOnTable[project_choiced].content))
                                Console.WriteLine("0. {0}", projectsOnTable[project_choiced].content.name);
                            if (!CheckProjectIsCompleted(projectsOnTable[project_choiced].design))
                                Console.WriteLine("1. {0}", projectsOnTable[project_choiced].design.name);
                            if (!CheckProjectIsCompleted(projectsOnTable[project_choiced].develop))
                                Console.WriteLine("2. {0}", projectsOnTable[project_choiced].develop.name);
                            if (!CheckProjectIsCompleted(projectsOnTable[project_choiced].manage))
                                Console.WriteLine("3. {0}", projectsOnTable[project_choiced].manage.name);
                            switch (CheckCoice(Console.ReadLine()))
                            {
                                case 0:
                                    if (!CheckProjectIsCompleted(projectsOnTable[project_choiced].content))
                                    {
                                        projectsOnTable[project_choiced].content.current++;
                                        list.RemoveAt(empl_choiced);
                                        job_made = true;
                                    }
                                    else
                                        Console.WriteLine("Простите, но на проекте {0} нет работы по {1}", projectsOnTable[project_choiced].name, projectsOnTable[project_choiced].content.name);
                                    break;
                                case 1:
                                    if (!CheckProjectIsCompleted(projectsOnTable[project_choiced].design))
                                    {
                                        projectsOnTable[project_choiced].design.current++;
                                        list.RemoveAt(empl_choiced);
                                        job_made = true;
                                    }
                                    else
                                        Console.WriteLine("Простите, но на проекте {0} нет работы по {1}", projectsOnTable[project_choiced].name, projectsOnTable[project_choiced].design.name);
                                    break;
                                case 2:
                                    if (!CheckProjectIsCompleted(projectsOnTable[project_choiced].develop))
                                    {
                                        projectsOnTable[project_choiced].develop.current++;
                                        list.RemoveAt(empl_choiced);
                                        job_made = true;
                                    }
                                    else
                                        Console.WriteLine("Простите, но на проекте {0} нет работы по {1}", projectsOnTable[project_choiced].name, projectsOnTable[project_choiced].develop.name);
                                    break;
                                case 3:
                                    if (!CheckProjectIsCompleted(projectsOnTable[project_choiced].manage))
                                    {
                                        projectsOnTable[project_choiced].manage.current++;
                                        list.RemoveAt(empl_choiced);
                                        job_made = true;
                                    }
                                    else
                                        Console.WriteLine("Простите, но на проекте {0} нет работы по {1}", projectsOnTable[project_choiced].name, projectsOnTable[project_choiced].manage.name);
                                    break;
                                default:
                                    Console.WriteLine("Такой работы нет в этом проекте");
                                    break;
                            }
                            break;
                        default:
                            break;
                    }
                }

            }
        }

        bool CheckProjectIsCompleted(CardsParts.Working work)
        {
            if (work.current == work.max)
                return true;
            else
                return false;
        }

        bool IsProjectCardInHand(int card_id)
        {
            foreach (CardsTypes.Project card in inHand.projects)
            {
                if (card.id == card_id)
                    return true;
            }
            Console.WriteLine("Простите, но такой карты не обнаружено у вас в руке");
            return false;
        }

        static int CheckCoice(string temp)
        {
            int t_choice;
            try
            {
                t_choice = Convert.ToInt32(temp);
                return t_choice;
            }
            catch
            {
                Console.WriteLine("Пожалуйста, вводите только цифру");
                return CheckCoice(Console.ReadLine());
            }
        }
    }

    class InHand
    {
        public List<CardsTypes.Project> projects;
        public List<CardsTypes.Chanses> chanses;

        public InHand()
        {
            projects = new List<CardsTypes.Project>();
            chanses = new List<CardsTypes.Chanses>();
        }
    }

    class GoldBalance
    {
        public int current;
        public int profit;
        public int add_profit;
        public int turn_without_profit;
        public int add;
        public int add_turns;
        public int lose;
        public int lose_turns;

        public GoldBalance()
        {
            current = 0;
            profit = 0;
            add_profit = 1; //Используется как множитель, чтобы вычислять - прибавлять профит или нет
            turn_without_profit = 0;
            add = 0;
            add_turns = 0;
            lose = 0;
            lose_turns = 0;
        }

        public void ProfitCalculate()
        {

        }

        public void GoldCalculate()
        {
            current = current + profit * Math.Max(0, (add_profit - turn_without_profit)) + add * Math.Min(1, add_turns) - lose * Math.Min(1, lose_turns);
        }
    }

    class ProjectsOnTable
    {
        //общий сборщик для информации по картам на столе, чтобы лишний раз не бегать по игрокам
        public static List<int> cards;
        public static bool featureAcuaride;
        public static List<int> features;
    }
}
