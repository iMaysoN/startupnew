﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StartupServerNew
{
    class GiveCard
    {
        public static void Projects (Player player, int count=1)
        {
            for (int i = 0; i < count; i++)
            {
                player.inHand.projects.Add(Vars.game_deck_project[0]);
                Vars.game_deck_project.RemoveAt(0);
            }
        }
        public static void Chanses(Player player, int count = 1)
        {
            for (int i = 0; i < count; i++)
            {
                player.inHand.chanses.Add(Vars.game_deck_chanses[0]);
                Vars.game_deck_chanses.RemoveAt(0);
            }
        }
    }
}
